import requests, re

response = requests.get("https://www.wikidata.org/w/api.php?action=query&format=json&prop=revisions&meta=&titles=Wikidata%3AWikidataCon_2019%2FProgram%2FSchedule&rvprop=content&rvlimit=1")

text = ""
for e in response.json()["query"]["pages"]:
    for f in response.json()["query"]["pages"][e]['revisions'] :
        text = f["*"]

res = re.findall("(\{\{[^\|]*(?:\|(?!show)[^\|]*)*\|show\=\{\{\{show\|Calendar\}\}\}(?:\|[^|}]+)*\}\})", text)

final = ""
for e in res:
    final += e + "\n"

f = open("/data/project/datacon-schedule-app/refactor-schedule/.extract.txt", "w+")
f.write(final)
f.close()

