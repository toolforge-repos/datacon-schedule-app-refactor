#!/bin/bash
python3 /data/project/datacon-schedule-app/refactor-schedule/refactor.py
python /data/project/shared/pywikibot/core/scripts/pagefromfile.py -textonly -title:"Wikidata:WikidataCon_2019/Program/Schedule/Sync" -file:/data/project/datacon-schedule-app/refactor-schedule/.extract.txt -summary:"[datacon-schedule-app] Bot update schedule sync" -force -always -log:/data/project/datacon-schedule-app/refactor-schedule/update.log -pt:0
